import java.util.List;

@FunctionalInterface
public interface OnSuperComputerUpdate {

    void onUpdate(SuperComputer sc, List<Employee> currentWorking, List<Employee> currentQueue);
}

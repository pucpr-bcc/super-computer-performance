import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Semaphore;

public class SuperComputer extends Thread {
    private boolean running = false;

    private final Semaphore semaphore;
    private final int capacity;

    private final Semaphore queueMutex = new Semaphore(1);
    private final List<Employee> queue = new LinkedList<>();

    private final Semaphore currentWorkerMutex = new Semaphore(1);
    private final List<Employee> activeWorkers = new LinkedList<>();

    private OnSuperComputerUpdate onUpdate;

    public SuperComputer(int workers) {
        this.capacity = workers;
        semaphore = new Semaphore(this.capacity);
    }

    @Override
    public void run() {
        while (this.running || !this.canTurnOff()) {
            tryAcquireAll(queueMutex, currentWorkerMutex);
            Employee next = getNextInLine();
            releaseAll(queueMutex, currentWorkerMutex);
            if (next != null) {
                tryWaitingAcquire(semaphore);
                new Thread(next).start();
                update();
            }

        }
    }

    private void tryAcquireAll(Semaphore... sem) {
        for (Semaphore s : sem) {
            tryWaitingAcquire(s);
        }
    }

    private void releaseAll(Semaphore... sem) {
        for (Semaphore s : sem) {
            s.release();
        }
    }

    public Employee getNextInLine() {
        if (activeWorkers.size() < this.capacity && !queue.isEmpty()) {
            return removeNextFromLists();
        }
        return null;
    }

    private Employee removeNextFromLists() {
        Employee next = getEmployeeSameCompany();
        if (next != null) {
            queue.remove(next);
            activeWorkers.add(next);
            return next;

        } else if (activeWorkers.size() == 0) {
            Employee empNewComp = queue.remove(0);
            activeWorkers.add(empNewComp);
            return empNewComp;
        }
        return null;
    }

    private Employee getEmployeeSameCompany() {
        Employee next = null;
        if (activeWorkers.isEmpty()) {
            next = queue.get(0);
        } else {
            for (Employee employee : queue) {
                if (activeWorkers.get(0).getCompany().equals(employee.getCompany())) {
                    next = employee;
                    break;
                }
            }
        }
        return next;
    }

    private void tryWaitingAcquire(Semaphore s) {
        try {
            s.acquire();
        } catch (InterruptedException ex) {
            System.out.println("Waiting for permits interrupted");
        }
    }

    public void enterQueue(final Employee e) {
        tryWaitingAcquire(queueMutex);
        queue.add(e);
        queueMutex.release();
        update();
    }

    public void finishWork(final Employee e) {
        tryWaitingAcquire(currentWorkerMutex);
        activeWorkers.remove(e);
        currentWorkerMutex.release();
        semaphore.release();
        update();
    }

    public void togglePower() {

        if (this.running) {
            System.out.println("Turning super computer off");
        } else {
            System.out.println("Turning super computer on");
        }

        this.running = !this.running;
    }

    public void setOnUpdate(OnSuperComputerUpdate onSuperComputerUpdate) {
        this.onUpdate = onSuperComputerUpdate;
    }

    private void update() {
        if (this.onUpdate != null) {
            tryWaitingAcquire(queueMutex);
            tryWaitingAcquire(currentWorkerMutex);
            this.onUpdate.onUpdate(this, new LinkedList<>(activeWorkers), new LinkedList<>(queue));
            queueMutex.release();
            currentWorkerMutex.release();
        }
    }

    public boolean canTurnOff() {
        tryWaitingAcquire(currentWorkerMutex);
        boolean ret = activeWorkers.size() == 0;
        currentWorkerMutex.release();

        tryWaitingAcquire(queueMutex);
        ret = ret && queue.size() == 0;
        queueMutex.release();

        return ret;
    }

    public int getCapacity() {
        return capacity;
    }
}

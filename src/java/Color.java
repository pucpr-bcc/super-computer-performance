public enum Color {
    RED("0;31m"),
    BLUE("0;34m"),
    GREEN("0;32m"),
    YELLOW("1;33m"),
    PURPLE("0;35m"),
    RESET("0;0m");

    private final String escape;

    Color(String escape) {
        this.escape = escape;
    }

    public String scape() {
        return this.escape;
    }
}

import java.util.Objects;
import java.util.Random;

public class Employee implements Runnable {
    private static final Random random = new Random();
    private final String name;
    private final Company company;

    public Employee(String name, Company company) {
        this.name = name;
        this.company = company;
    }

    public String getName() {
        return name;
    }


    public Company getCompany() {
        return company;
    }

    @Override
    public void run() {
        try {
            Thread.sleep(randomWorkTime());
            company.getSuperComputer().finishWork(this);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Employee employee = (Employee) o;
        return name.equals(employee.name) && company.equals(employee.company);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, company);
    }

    @Override
    public String toString() {
        return name + " from " + company;
    }

    private int randomWorkTime() {
        return random.nextInt(5000) + 5000;
    }
}

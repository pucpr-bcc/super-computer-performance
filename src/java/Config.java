import java.util.List;

public class Config {
    private final List<String> companiesNames;
    private final int capacity;
    private final int minWorker;
    private final int maxWorker;

    public Config(List<String> companiesNames, int capacity, int minWorker, int maxWorker) {
        this.companiesNames = companiesNames;
        this.capacity = capacity;
        this.minWorker = minWorker;
        this.maxWorker = maxWorker;
    }

    public List<String> getCompaniesNames() {
        return companiesNames;
    }

    public int getCapacity() {
        return capacity;
    }

    public int getMinWorker() {
        return minWorker;
    }

    public int getMaxWorker() {
        return maxWorker;
    }
}

import java.util.*;

/*
Allan Braun
Gabriel Przytocki
Matheus Bertho
Pedro Contessoto
Valdemar Ceccon
*/

public class Main {

    public static void main(String[] args) throws InterruptedException {
        Config configuration = readConfigFromUser();
        Random r = new Random();
        SuperComputer sc = new SuperComputer(configuration.getCapacity());
        sc.setOnUpdate(Main::display);
        List<Company> companyList = new LinkedList<>();
        int bound = configuration.getMaxWorker() - configuration.getMinWorker();
        for (String companiesName : configuration.getCompaniesNames()) {
            companyList.add(createCompany(sc, companiesName, (bound == 0 ? 0 : r.nextInt(bound)) + configuration.getMinWorker()));
        }

        sc.togglePower();
        sc.start();

        for (Company company : companyList) {
            company.start();
        }

        for (Company company : companyList) {
            company.join();
        }

        sc.join();

        System.out.println("\033[2J");
        System.out.println("Every company finished working and queue is empty. Turning off supercomputer");
        sc.togglePower();

    }

    private static Config readConfigFromUser() {
        Scanner s = new Scanner(System.in);
        String cNames = readStringForUser(s, "A,B");
        int capacity = readIntFromUser("Super computer capacity", 1, 3, "Capacity must be 1 or greater", s);
        int min = readIntFromUser("Minimum employees by company", 1, 10, "Min must be %d or greater", s);
        int max = readIntFromUser("Maximum employees by company", min, 10, "Capacity must be %d or greater", s);
        return new Config(Arrays.asList(cNames.split(",")), capacity, min, max);
    }

    private static int readIntFromUser(String prompt, int min, int defaultValue, String error, Scanner s) {
        boolean valid = false;
        int ret = 0;
        do {
            System.out.printf("%s (min: %d, default: %d): ", prompt, min, defaultValue);
            String capString = s.nextLine().trim();
            if (capString.isEmpty())
                return defaultValue;
            try {
                ret = Integer.parseInt(capString);
                if (ret >= min) {
                    valid = true;
                } else {
                    System.out.printf(error + "%n", min);
                }
            } catch (Exception e) {
                System.out.printf(error + "%n", min);
            }
        } while (!valid);
        return ret;
    }

    private static String readStringForUser(Scanner s, String defaultValue) {
        String cNames;
        boolean valid = false;
        do {
            System.out.printf("Companies names separated by ,%s: ", defaultValue.isEmpty() ? "" : "(default: "+defaultValue+")");
            cNames = s.nextLine().trim();
            if (cNames.length() > 0)
                valid = true;
            else if (defaultValue.length() > 0) {
                return defaultValue;
            }
            else {
                System.out.println("Minimum of 1 company name");
            }
        } while (!valid);
        return cNames;
    }

    public static void display(SuperComputer sc, List<Employee> currentWorking, List<Employee> currentQueue) {
        System.out.print("\033[2J");
        System.out.flush();
        System.out.printf("Capacity: %d%n", sc.getCapacity());
        System.out.println("Working:");
        for (Employee employee : currentWorking) {
            System.out.printf("\33[%s  %s\33[%s%n", employee.getCompany().c.scape(), employee, Color.RESET.scape());
        }
        System.out.println();
        System.out.println("Queue:");
        for (Employee employee : currentQueue) {
            System.out.printf("\33[%s  %s\33[%s%n", employee.getCompany().c.scape(), employee, Color.RESET.scape());
        }

    }

    public static Company createCompany(SuperComputer sc, String name, int qtEmployees) {
        Company company = new Company(sc, name);
        for (int i = 0; i < qtEmployees; i++) {
            company.addEmployee(String.format("Employee %d", i + 1));
        }

        return company;
    }
}

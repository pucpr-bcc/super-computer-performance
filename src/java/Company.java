import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Random;

public class Company extends Thread {
    public static int COLOR_INDEX = 0;
    public final Color c;
    public static final Random r = new Random();
    private final SuperComputer superComputer;
    private final List<Employee> employees;
    private final String compName;

    public Company(SuperComputer sc, String name) {
        this.superComputer = sc;
        this.employees = new LinkedList<>();
        this.compName = name;
        this.c = Color.values()[(COLOR_INDEX++) % Color.values().length];
    }

    public void addEmployee(String name) {
        employees.add(new Employee(name, this));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Company company = (Company) o;
        return compName.equals(company.compName);
    }

    public SuperComputer getSuperComputer() {
        return superComputer;
    }

    @Override
    public void run() {
        for (Employee e : employees) {
            try {
                long delay = (Math.abs(r.nextLong()) % 150) + 300;
                superComputer.enterQueue(e);
                Thread.sleep(delay);
            } catch (InterruptedException ex) {
                System.out.printf("%s interrupted", this);
            }
        }
    }

    @Override
    public int hashCode() {
        return Objects.hash(compName);
    }

    public String getCompName() {
        return compName;
    }

    @Override
    public String toString() {
        return "Company " + compName;
    }
}
